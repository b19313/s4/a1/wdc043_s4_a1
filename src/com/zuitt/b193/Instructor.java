package com.zuitt.b193;

public class Instructor {

    private String firstName;
    private String lastName;
    private int age;
    private String address;

   ;

    public  Instructor (){

    };




    public Instructor (String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    };

    //! SETTER FOR INSTRUCTOR
    public void setFirstName(String firstName){
        this.firstName = firstName;

    }
    public  void  setLastName (String lastName){
        this.lastName = lastName;
    }

    public void  setAge(int age){
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    //! Getter

    public String getFirstName (){
        return this.firstName;
    }
    public  String getLastName (){
        return  this.lastName;
    }

    public int getAge (){
        return  this.age;
    }

    public String getAddress(){
        return this.address;
    }
}
