package com.zuitt.b193;

public class Courses {

    private String course_name;
    private String course_description;
    private int course_seat;
    private String course_instructors_firstname;

    public Courses(){};


    //! GETTERS
    public String getCourse_name(){
        return this.course_name;
    }
    public String getCourse_description(){
        return this.course_description;
    }
    public int getCourse_seat(){
        return this.course_seat;
    }
    public  String getCourse_instructors_firstname(){
        return  this.course_instructors_firstname;
    }



    //! setters
    public void setCourse(String course_name){
        this.course_name = course_name;

    }
    public void setDescription(String course_description){
        this.course_description = course_description;
    }
    public void setSeats (int course_seat){
        this.course_seat = course_seat;
    }
    public void setCourseFN (String course_instructors_firstname){
        this.course_instructors_firstname = course_instructors_firstname;
    }



}
