package com.zuitt.b193;

public class Main {
    public static void main (String[] args){



        Instructor myInstructor = new Instructor();
        myInstructor.setFirstName("Judylyn");
        myInstructor.setLastName("Medalla");
        myInstructor.setAge(27);
        myInstructor.setAddress("Metro Manila");


        Courses myCourse = new Courses();
        myCourse.setCourse("Web Development");
        myCourse.setDescription("MERN STACK WEB DEVELOPMENT");
        myCourse.setSeats(30);
        myCourse.setCourseFN("Judylyn");


        System.out.println("User's first name: ");
        System.out.println(myInstructor.getFirstName());
        System.out.println("User's last name:");
        System.out.println(myInstructor.getLastName());
        System.out.println("User's age:");
        System.out.println(myInstructor.getAge());
        System.out.println("User's address: ");
        System.out.println(myInstructor.getAddress());
        System.out.println("Course name:");
        System.out.println(myCourse.getCourse_name());
        System.out.println("Course Description:");
        System.out.println(myCourse.getCourse_description());
        System.out.println("Course available seats");
        System.out.println(myCourse.getCourse_seat());
        System.out.println("Instructors First Name");
        System.out.println(myCourse.getCourse_instructors_firstname());



    }
}
